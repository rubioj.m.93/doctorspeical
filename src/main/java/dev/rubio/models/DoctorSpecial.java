package dev.rubio.models;

import java.io.Serializable;
import java.util.Objects;

public class DoctorSpecial implements Serializable {
    private int id;
    private String speciality;
    private String insurance;
    private String name;



    public DoctorSpecial(int id, String speciality,String insurance, String name){
        this.id = id;
        this.speciality = speciality;
        this.insurance = insurance;
        this.name = name;
    }



    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSpeciality() {
        return speciality;
    }

    public void setSpeciality(String speciality) {
        this.speciality = speciality;
    }

    public String getInsurance() {
        return insurance;
    }

    public void setInsurance(String insurance) {
        this.insurance = insurance;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DoctorSpecial that = (DoctorSpecial) o;
        return id == that.id && Objects.equals(speciality, that.speciality) && Objects.equals(insurance, that.insurance) && Objects.equals(name, that.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, speciality, insurance, name);
    }

    @Override
    public String toString() {
        return "DoctorSpecial{" +
                "id=" + id +
                ", speciality='" + speciality + '\'' +
                ", insurance='" + insurance + '\'' +
                ", name='" + name + '\'' +
                '}';
    }
}
